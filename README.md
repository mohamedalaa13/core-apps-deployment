## Run Get_IIS_Configurations
1. To get the IIS configurations of this Server.
2. Copy These Configurations to Single Place (Jenkins Server).



## Upload Deployment Package to Nexus Repository Artifact (Snapshot)
   `EX: curl -v --user 'xxx-xxx:xxx-xxx' --upload-file ./MinicashAPIs.zip http://10.1.1.52:8081/repository/MinicashAPIs-Snapshot/MinicashAPIs_14-05-2023/MinicashAPIs.zip`


## Run Pre-Deployment Pipeline
1. Download Snapshot Deployment.
2. Run Deployment Validation on this package.



## Run Deployment Pipeline
1. When Pre-Deployment Pipelin is successful.
2. Copy Deployment Package into target Deployment Windows Server.
3. Clone the original production Web.config File into package instead of deployment one.
4. Backup the old Deployment Version.
5. Deploy the New Deployment Version on IIS and test New Deployment.
6. If New Deployment Test is failure, Trigger the Rollback.
